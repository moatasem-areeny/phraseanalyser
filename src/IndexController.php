<?php

namespace Elareeny\Phraseanalyser;

use App\Http\Controllers\Controller;

use Illuminate\Http\Request;

class IndexController extends Controller
{

    public function index(){

        return view('phraseanalyser::index');

    }

    public function result(Request $request){

        $this->validate($request,[

            'customer_string'=>'required|max:255'

        ]);
        $postRequist = $request->input("customer_string");

            $result = $this->getGridData($postRequist);

          return view('phraseanalyser::result',compact('result'));

    }

    protected function getGridData($postRequist){

        //remove spaces from the post request string

        $string = str_replace(" ","",$postRequist);

        //split string to characters array

        $charactersArray = str_split($string);
        //create an array to collect all the required data
        $characterInfoArray = array();
        for ($i=0; $i < count($charactersArray);$i++){


            if (array_key_exists($charactersArray[$i] , $characterInfoArray )){

                $before = isset($charactersArray[$i-1]) ? $charactersArray[$i-1]:"none";
                $after = isset($charactersArray[$i+1]) ? $charactersArray[$i+1]:"none";
                $characterInfoArray[$charactersArray[$i]]['before'] = $before. ",".$characterInfoArray[$charactersArray[$i]]['before'];
                $characterInfoArray[$charactersArray[$i]]['after'] = $after. ",". $characterInfoArray[$charactersArray[$i]]['after'];
                $characterInfoArray[$charactersArray[$i]]['count'] ++;
                $characterInfoArray[$charactersArray[$i]]['distance'] = $i - $characterInfoArray[$charactersArray[$i]]['first_key'];

            }else{
                $characterInfoArray[$charactersArray[$i]]['before'] = isset($charactersArray[$i-1]) ? $charactersArray[$i-1]:"none";
                $characterInfoArray[$charactersArray[$i]]['after'] = isset($charactersArray[$i+1]) ? $charactersArray[$i+1]:"none";
                $characterInfoArray[$charactersArray[$i]]['count'] = 1;

            }
            $characterInfoArray[$charactersArray[$i]]['first_key'] = isset($characterInfoArray[$charactersArray[$i]]['first_key'])? $characterInfoArray[$charactersArray[$i]]['first_key'] : $i ;


        }



        return $characterInfoArray;


    }
}
