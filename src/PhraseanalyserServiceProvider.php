<?php

namespace Elareeny\Phraseanalyser;

use Illuminate\Support\ServiceProvider;

class PhraseanalyserServiceProvider extends ServiceProvider
{
    /**
     * Register services.
     *
     * @return void
     */
    public function register()
    {
        $this->app->make('Elareeny\Phraseanalyser\IndexController');

        $this->loadViewsFrom(__DIR__.'/views', 'phraseanalyser');

    }

    /**
     * Bootstrap services.
     *
     * @return void
     */
    public function boot()
    {
        include __DIR__ . '/routes.php';

    }
}
