<html>
<head>
    <script type="text/javascript" src="https://www.gstatic.com/charts/loader.js"></script>
    <script type="text/javascript">
        google.charts.load('current', {'packages':['bar']});
        google.charts.setOnLoadCallback(drawChart);

        function drawChart() {
            var data = google.visualization.arrayToDataTable([
                ['characters', 'Character Count', 'Max Distance'],
                @foreach ($result as $key => $characterInfo)

                ["<?php echo $key ?> (Before : <?php echo $characterInfo['before'] ?> after : <?php echo $characterInfo['after'] ?>)" , "<?php echo $characterInfo['count'] ?>", "<?php echo isset($characterInfo['distance']) ? $characterInfo['distance']:null ?>"],

                @endforeach

            ]);

            var options = {
                chart: {
                    title: 'pharase Analyzer Chart',
                }
            };

            var chart = new google.charts.Bar(document.getElementById('columnchart_material'));

            chart.draw(data, google.charts.Bar.convertOptions(options));
        }
    </script>
</head>
<body>
<div id="columnchart_material" style="width: 1800px;height: 500px;"></div>
</body>
</html>

